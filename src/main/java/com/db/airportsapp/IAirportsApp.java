package com.db.airportsapp;

import java.util.List;

public interface IAirportsApp {
    List<Airport> findAirportByCode(String code);

    List<Airport> findAirportByName(String name);
}
