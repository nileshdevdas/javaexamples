package com.db.mocking;

/**
 * @author Nilesh
 * @since 1.0
 * CUT Class under test
 * My Bank implementation need certain dependencies
 */
public class BankImpl implements IBank {
    /**
     * THe Dependency ......
     */
    IBankService service = null;

    public BankImpl(IBankService service) {
        this.service = service;
    }

    /**
     * Method under test
     * @param amount
     * @param accountid
     * @return
     */
    @Override
    public String deposit(double amount, String accountid) {
        if (amount <= 0) {
            throw new IllegalArgumentException("amount cannot be 0 or negative");
        }
        if (accountid == null || accountid.length() == 0) {
            throw new IllegalArgumentException("Account id is mandatory and cannot be blank");
        }
        if(amount > 50000) {
            System.out.println("Here you are going to further check the Authirization using otp");
        }
        return service.transaction(TransactionType.CREDIT, accountid, amount);
    }

    @Override
    public String withdraw(double amount, String accountid) {
        if (amount <= 0) {
            throw new IllegalArgumentException("amount cannot be 0 or negative");
        }
        if (accountid == null || accountid.length() == 0) {
            throw new IllegalArgumentException("Account id is mandatory and cannot be blank");
        }
        if(amount > 50000) {
            System.out.println("Here you are going to further check the Authirization using otp");
        }
        return service.transaction(TransactionType.DEBIT, accountid, amount);
    }
}
