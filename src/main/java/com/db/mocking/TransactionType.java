package com.db.mocking;

/**
 * @author  Parag
 */
public enum TransactionType {
    DEBIT, CREDIT
}
